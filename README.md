# TodoList project with react typescript

This project use template typescript :
```bash
npx create-react-app . --template typescript
```

## Dependencies
    - react | typescript
    - react-router-dom | @types/react-router-dom
    - @reduxjs/toolkit | react-redux
    - @mswjs/data
    - node-sass

## Clone this project

```bash
git clone https://gitlab.com/mlltmax66/todolist-reduxtoolkit-react-typescript
```

```bash
cd todolist-reduxtoolkit-react-typescript
```

## Launch this project

install all dependencies:
```bash
yarn install
```

launch this project:
```bash
yarn start
```

launch test:
```bash
yarn test
```
