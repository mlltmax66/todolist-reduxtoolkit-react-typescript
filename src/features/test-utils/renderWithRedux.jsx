import { render as rtlRender } from '@testing-library/react'
import { configureStore } from '@reduxjs/toolkit'
import { Provider } from 'react-redux'
import { todosApi } from '../../app/services/todosApi'
import toastSlice from '../../app/slices/toastSlice'

function renderWithRedux(
  ui,
  {
    preloadedState,
    store = configureStore({ 
        reducer: { 
            [todosApi.reducerPath]: todosApi.reducer,
            [toastSlice.name]: toastSlice.reducer
        }, 
        middleware: (defaultMiddleware) => defaultMiddleware().concat(todosApi.middleware),
        preloadedState 
    }),
    ...renderOptions
  } = {}
) {
  function Wrapper({ children }) {
    return <Provider store={store}>{children}</Provider>
  }
  return rtlRender(ui, { wrapper: Wrapper, ...renderOptions })
}


export { renderWithRedux }