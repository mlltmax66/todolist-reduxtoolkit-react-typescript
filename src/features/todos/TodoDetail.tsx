import { nanoid } from '@reduxjs/toolkit'
import React, { useState } from 'react'
import { useAppDispatch } from '../../app/hooks'
import { Todo, useDeleteTodoMutation, useUpdateTodoMutation } from '../../app/services/todosApi'
import { addToast } from '../../app/slices/toastSlice'

const TodoDetail: React.FC<{todo: Todo}> = ({todo}) => {
    const dispatch = useAppDispatch()
    const [edit, setEdit] = useState<boolean>(false)
    const [name, setName] = useState<string>(todo.name)
    const [updateTodo] = useUpdateTodoMutation()
    const [deleteTodo] = useDeleteTodoMutation()

    const handleEdit = () => {
        setEdit(v => !v)
        setName(todo.name)
    }

    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setName(e.target.value)
    }

    const handleSave = async() => {
        if(name.trim() !== '') {
            try {
                await updateTodo({id: todo.id, name})
                dispatch(addToast({
                    id: nanoid(),
                    text: 'todo has been modified',
                    type: 'success'
                }))
            } catch {
                dispatch(addToast({
                    id: nanoid(),
                    text: 'This service is not available at the moment',
                    type: 'error'
                }))
            }
            setEdit(v => !v)
        }
    }

    const handleDelete = async() => {
        try {
            await deleteTodo(todo.id)
            dispatch(addToast({
                id: nanoid(),
                text: 'todo has been deleted',
                type: 'success'
            }))
        } catch {
            dispatch(addToast({
                id: nanoid(),
                text: 'This service is not available at the moment',
                type: 'error'
            }))
        }
    }

    return (
        <li className="todoDetail">
            {edit ? <>
                <input type="text" value={name} onChange={handleChange} className="todoDetail__input" />
                <div className="todoDetail-wrapper">
                    <button className="todoDetail__button save" data-testid="todo-save" onClick={handleSave}><i className="far fa-save"></i></button>
                    <button className="todoDetail__button cancel" data-testid="todo-cancel" onClick={handleEdit}><i className="fas fa-times"></i></button>
                </div>
            </>
            : <>
                <p className="todoDetail__name">{todo.name}</p>
                <div className="todoDetail-wrapper">
                    <button className="todoDetail__button edit" data-testid="todo-edit" onClick={handleEdit}><i className="far fa-edit"></i></button>
                    <button className="todoDetail__button delete" data-testid="todo-delete" onClick={handleDelete}><i className="far fa-trash-alt"></i></button>
                </div>
            </>}
        </li>
    )
}

export default TodoDetail
