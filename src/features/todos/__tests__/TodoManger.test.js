import { screen, fireEvent, waitFor } from "@testing-library/react"
import { rest } from "msw";
import server from "../../mocks/server";
import { renderWithRedux } from "../../test-utils/renderWithRedux"
import TodoManager from "../TodoManager"

describe('TodoManager component', () => {

    beforeAll(() => server.listen())

    afterEach(() => server.resetHandlers())

    afterAll(() => server.close())

    test('Should an message, if after fetching todos, todos[] is empty', async() => {
        server.use(
            rest.get('/todos', (req, res, ctx) => {
                return res(
                    ctx.status(200),
                    ctx.json([]),
                    ctx.delay(150)
                )
            })
        )
        renderWithRedux(<TodoManager />)
        const loading = screen.getByText(/Loading.../i)
        expect(loading).toBeInTheDocument()
        expect(await screen.findByText('No todos :(')).toBeInTheDocument()
    })

    test('Should an message error, if have an error fetching todos', async () => {
        server.use(
            rest.get('/todos', (req, res, ctx) => {
                return res(
                    ctx.status(500),
                    ctx.delay(150)
                )
            })
        )
        renderWithRedux(<TodoManager />)
        const loading = screen.getByText(/Loading.../i)
        expect(loading).toBeInTheDocument()
        expect(await screen.findByText('This service is not available at the moment'))
    })

    test('Should an list of todos, after fetching todos', async() => {
        renderWithRedux(<TodoManager />)
        const loading = screen.getByText(/Loading.../i)
        expect(loading).toBeInTheDocument()
        const todos = await screen.findAllByRole('listitem')
        expect(todos).toHaveLength(3)
        expect(todos[0]).toBeInTheDocument()
        expect(todos[1]).toBeInTheDocument()
        expect(todos[2]).toBeInTheDocument()
        expect(todos[0]).toHaveTextContent('first todo')
        expect(todos[1]).toHaveTextContent('second todo')
        expect(todos[2]).toHaveTextContent('third todo')
    })

    test('Should have an input add todo and button', () => {
        renderWithRedux(<TodoManager />)
        expect(screen.getByLabelText('Add todo')).toBeInTheDocument()
        expect(screen.getByRole('button', { name: 'Add' })).toBeInTheDocument()
    })

    test('Should change input value add todo, when typing in input', () => {
        renderWithRedux(<TodoManager />)
        const inputAddTodo = screen.getByLabelText('Add todo')
        expect(inputAddTodo).toBeInTheDocument()
        expect(inputAddTodo.value).toBe('')
        fireEvent.change(inputAddTodo, { target: { value: 'azeaze' } })
        expect(inputAddTodo.value).toBe('azeaze')
    })

    test('Should not have new todo in todo list, when submiting add todo form input is empty', async() => {
        renderWithRedux(<TodoManager />)
        const button = screen.getByRole('button', { name: 'Add' })
        fireEvent.click(button)
        expect(await screen.findAllByRole('listitem')).toHaveLength(3)
    })

    test('Should have new todo in todo list and have an toast success, when submiting add todo form successful', async() => {
        renderWithRedux(<TodoManager />)
        const inputAddTodo = screen.getByLabelText('Add todo')
        const button = screen.getByRole('button', { name: 'Add' })
        fireEvent.change(inputAddTodo, { target: { value: 'new todo' } })
        fireEvent.click(button)
        expect(await screen.findByText('new todo')).toBeInTheDocument()
        expect(await screen.findByText('new todo')).toHaveTextContent('new todo')
        expect(await screen.findByText('todo has been created')).toBeInTheDocument()
        window.setTimeout(() => expect(screen.queryByText('todo has been created')).toBeNull(), 3000)       
    })

    test('Should not have new todo in todo list and have an toast error, when submiting add todo form error', async() => {
        renderWithRedux(<TodoManager />)
        const inputAddTodo = screen.getByLabelText('Add todo')
        const button = screen.getByRole('button', { name: 'Add' })
        fireEvent.change(inputAddTodo, { target: { value: 'error' } })
        fireEvent.click(button)
        await waitFor(() => {
            expect(screen.queryByText('error')).toBeNull()
            expect(screen.queryByText('This service is not available at the moment')).toBeInTheDocument()
        })
        window.setTimeout(() => expect(screen.queryByText('This service is not available at the moment')).toBeNull(), 3000)       
    })

    test('Should add todo input value reset after submiting add todo form successful', async() => {
        renderWithRedux(<TodoManager />)
        const inputAddTodo = screen.getByLabelText('Add todo')
        const button = screen.getByRole('button', { name: 'Add' })
        fireEvent.change(inputAddTodo, { target: { value: 'new todo' } })
        fireEvent.click(button)
        await waitFor(() => expect(inputAddTodo.value).toBe(''))
    })

    test('Should not change todo name, when edit todo and save with input value empty', async() => {
        renderWithRedux(<TodoManager />)
        const editButton = await screen.findAllByTestId('todo-edit')
        fireEvent.click(editButton[0])
        expect(screen.getByTestId('todo-cancel')).toBeInTheDocument()
        const inputEditTodo = screen.getByRole('textbox', {name: ''})
        fireEvent.change(inputEditTodo, { target: { value: '' } })
        fireEvent.click(screen.getByTestId('todo-save'))
        expect(screen.getByTestId('todo-cancel')).toBeInTheDocument()
        expect(inputEditTodo.value).toBe('')
    })

    test('Should change todo name, leave edit mode and have an toast success, when save after edit todo successful', async() => {
        renderWithRedux(<TodoManager />)
        const editButton = await screen.findAllByTestId('todo-edit')
        fireEvent.click(editButton[0])
        expect(screen.getByTestId('todo-cancel')).toBeInTheDocument()
        const inputEditTodo = screen.getByRole('textbox', {name: ''})
        fireEvent.change(inputEditTodo, { target: { value: 'azeaze' } })
        fireEvent.click(screen.getByTestId('todo-save'))
        expect(await screen.findByText('azeaze')).toBeInTheDocument()
        expect(screen.queryByText('mock todo')).toBeNull()
        expect(screen.queryByTestId('todo-cancel')).toBeNull()
        expect(await screen.findByText('todo has been modified')).toBeInTheDocument()
        window.setTimeout(() => expect(screen.queryByText('todo has been modified')).toBeNull(), 3000) 
    })

    test('Should not have todo and have an toast success, when click on delete todo button successful', async() => {
        renderWithRedux(<TodoManager />)
        expect(await screen.findByText('second todo')).toBeInTheDocument()
        const deleteButton = await screen.findAllByTestId('todo-delete')
        fireEvent.click(deleteButton[1])
        await waitFor(() => {
            expect(screen.queryByText('second todo')).toBeNull()
        })
        expect(await screen.findByText('todo has been deleted')).toBeInTheDocument()
        window.setTimeout(() => expect(screen.queryByText('todo has been deleted')).toBeNull(), 3000) 
    })
})