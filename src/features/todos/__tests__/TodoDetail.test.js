import { screen, fireEvent} from "@testing-library/react"
import { renderWithRedux } from "../../test-utils/renderWithRedux"
import TodoDetail from "../TodoDetail"

const mockTodo = {
    id: 'id1',
    name: 'mock todo'
}

describe('TodoDetail component', () => {
    test('Should have name of todo and edit, delete button by default', () => {
        renderWithRedux(<TodoDetail todo={mockTodo} />)
        expect(screen.getByText('mock todo')).toBeInTheDocument()
        expect(screen.getByTestId('todo-edit')).toBeInTheDocument()
        expect(screen.getByTestId('todo-delete')).toBeInTheDocument()
    })

    test('Should switch on edit mode, when click on edit button', () => {
        renderWithRedux(<TodoDetail todo={mockTodo} />)
        expect(screen.getByText('mock todo')).toBeInTheDocument()
        expect(screen.getByTestId('todo-edit')).toBeInTheDocument()
        expect(screen.getByTestId('todo-delete')).toBeInTheDocument()
        expect(screen.queryByRole('textbox')).toBeNull()
        fireEvent.click(screen.getByTestId('todo-edit'))
        expect(screen.getByRole('textbox')).toBeInTheDocument()
        expect(screen.getByTestId('todo-cancel')).toBeInTheDocument()
        expect(screen.getByTestId('todo-save')).toBeInTheDocument()
        fireEvent.click(screen.getByTestId('todo-cancel'))
        expect(screen.getByText('mock todo')).toBeInTheDocument()
        expect(screen.getByTestId('todo-edit')).toBeInTheDocument()
        expect(screen.getByTestId('todo-delete')).toBeInTheDocument()
        expect(screen.queryByRole('textbox')).toBeNull()
    })

    test('Should by default edit todo input have todo name in value and change value, When typing in input', () => {
        renderWithRedux(<TodoDetail todo={mockTodo} />)
        fireEvent.click(screen.getByTestId('todo-edit'))
        const inputEditTodo = screen.getByRole('textbox')
        expect(inputEditTodo.value).toBe('mock todo')
        fireEvent.change(inputEditTodo, { target: { value: 'azeaze' } })
        expect(inputEditTodo.value).toBe('azeaze')
    })
})