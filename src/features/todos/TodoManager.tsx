import { nanoid } from '@reduxjs/toolkit'
import React, { useState } from 'react'
import { useAppDispatch } from '../../app/hooks'
import { useAddTodoMutation, useGetTodosQuery } from '../../app/services/todosApi'
import { addToast } from '../../app/slices/toastSlice'
import Toast from '../components/Toast'
import TodoDetail from './TodoDetail'

const TodosList: React.FC = () => {
    const { data: todos, isLoading, error } = useGetTodosQuery()

    if (isLoading) {
        return <p className="todoList__message">Loading...</p>
    }

    if(error) {
        return <p className="todoList__message">This service is not available at the moment</p>
    }

    if(!todos?.length) {
        return <p className="todoList__message">No todos :(</p>
    }

    return (
        <ul className="todoList__container">
            {todos?.map(todo => (
                <TodoDetail key={todo.id} todo={todo} />
            ))}
        </ul>
    )
}

const TodoManager: React.FC = () => {
    const dispatch = useAppDispatch()
    const [value, setValue] = useState<string>('')
    const [addTodo] = useAddTodoMutation()

    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setValue(e.target.value)
    }

    const handleSubmit = async(e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault()
        if(value.trim() !== '') {
            try {
                await addTodo({ name: value }).unwrap()
                dispatch(addToast({
                    id: nanoid(),
                    text: 'todo has been created',
                    type: 'success'
                }))    
            } catch {
                dispatch(addToast({
                    id: nanoid(),
                    text: 'This service is not available at the moment',
                    type: 'error'
                }))  
            }
            setValue('')
        }
    }

    return (
        <main>
            <div className="container">
                <form className="todoManager__form" onSubmit={handleSubmit}>
                    <div>
                        <label htmlFor="addTodo">Add todo</label>
                        <input type="text" id="addTodo" value={value} onChange={handleChange} />
                    </div>
                    <button type="submit">Add</button>
                </form>
                <TodosList />
            </div>
            <Toast autoDeleteInterval={3000} />
        </main>
    )
}

export default TodoManager
