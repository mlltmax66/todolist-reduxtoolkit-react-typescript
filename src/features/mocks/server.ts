import { setupServer } from 'msw/node'
import { handlers } from './db'

const server = setupServer(...handlers)

export default server