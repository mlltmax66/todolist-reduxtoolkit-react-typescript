import { factory, primaryKey } from '@mswjs/data'
import { nanoid } from '@reduxjs/toolkit'
import { rest } from 'msw'
import { Todo } from '../../app/services/todosApi'

const mockTodos = [
    {
        id: nanoid(),
        name: 'first todo'
    },
    {
        id: nanoid(),
        name: 'second todo'
    },
    {
        id: nanoid(),
        name: 'third todo'
    }
]

const db = factory({
    todo: {
        id: primaryKey(String),
        name: String
    }
})

mockTodos.forEach(todo => db.todo.create(todo))

export const handlers = [
    rest.post('/todos', async(req, res, ctx) => {
        const { name } = req.body as Partial<Todo>
  
        if (name === 'error') {
            return res(
                ctx.status(500),
                ctx.delay(150)
            )
        }
  
        const todo = db.todo.create({
            id: nanoid(),
            name
        })
    
        return res(
            ctx.status(201),
            ctx.json(todo), 
            ctx.delay(150)
        )
    }),
    rest.put('/todos/:id', async(req, res, ctx) => {
        const { name } = req.body as Partial<Todo>
   
        const todo = db.todo.update({
          where: { id: { equals: req.params.id } },
          data: { name }
        })
    
        return res(ctx.json(todo), ctx.delay(150))
    }),
    ...db.todo.toHandlers('rest')
]


