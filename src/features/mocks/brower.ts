import { setupWorker } from 'msw'
import { handlers } from './db'

const worker = setupWorker(...handlers)

export default worker