import React from 'react'
import { useAppDispatch, useAppSelector } from '../../app/hooks';
import { deleteToast } from '../../app/slices/toastSlice';

const Toast: React.FC<{ autoDeleteInterval: number }> = ({autoDeleteInterval}) => {
    const toasts =  useAppSelector(state => state.toast)
    const dispatch = useAppDispatch()

    return (
      <div className='toast'>
        {toasts?.map((toast) => {
            window.setTimeout(() => {
                dispatch(deleteToast(toast.id))
            }, autoDeleteInterval)
          
            return (
                <div className={toast.type === 'success' ? 'toast-wrapper success' : 'toast-wrapper error'} key={toast.id}>
                    <p>{toast.text}</p>
                </div>
            )
        })}
      </div>
    )
}

export default Toast