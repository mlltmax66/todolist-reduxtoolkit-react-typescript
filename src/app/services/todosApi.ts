import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'

export interface Todo {
    id: string,
    name: string
}

type TodosResponse = Todo[]

export const todosApi = createApi({
    baseQuery: fetchBaseQuery({ baseUrl: '/' }),
    tagTypes: ['Todo'],
    endpoints: (build) => ({
        getTodos: build.query<TodosResponse, void>({
            query: () => 'todos',
            providesTags: (result) => result ? [
                    ...result.map(({id}) => ({ type: 'Todo' as const, id })),
                    { type: 'Todo', id: 'List' }
                ] : [{ type: 'Todo', id: 'List' }]
        }),
        addTodo: build.mutation<Todo, Partial<Todo>>({
            query: (name) => ({
                url: 'todos',
                method: 'post',
                body: name
            }),
            invalidatesTags: [{ type: 'Todo', id: 'List' }]
        }),
        updateTodo: build.mutation<void, Pick<Todo, 'id'> & Partial<Todo>>({
            query: ({ id, ...patch }) => ({
              url: `todos/${id}`,
              method: 'put',
              body: patch,
            }),
            invalidatesTags: (result, error, { id }) => [{ type: 'Todo', id }],
        }),
        deleteTodo: build.mutation<{ success: boolean; id: string }, string>({
            query(id) {
              return {
                url: `todos/${id}`,
                method: 'delete',
              }
            },
            invalidatesTags: (result, error, id) => [{ type: 'Todo', id }],
        })
    })
})

export const { useGetTodosQuery, useAddTodoMutation, useUpdateTodoMutation, useDeleteTodoMutation } = todosApi