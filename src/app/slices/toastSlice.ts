import { createSlice, PayloadAction } from '@reduxjs/toolkit'

interface Toast {
    id: string,
    type: 'success' | 'error'
    text: string
}

const toastSlice = createSlice({
    name: 'toast',
    initialState: [] as Toast[],
    reducers: {
        addToast: (state, action: PayloadAction<Toast>) => { 
            state.push(action.payload)
        },
        deleteToast: (state, action: PayloadAction<string>) => { 
            return state.filter(toast => toast.id !== action.payload)
        }
    },
})

export default toastSlice

export const { addToast, deleteToast } = toastSlice.actions