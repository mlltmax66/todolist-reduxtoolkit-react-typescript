import { configureStore }  from '@reduxjs/toolkit'
import { todosApi } from './services/todosApi'
import toastSlice from './slices/toastSlice'

const store = configureStore({
    reducer: {
        [todosApi.reducerPath]: todosApi.reducer,
        [toastSlice.name]: toastSlice.reducer
    },
    middleware: (defaultMiddleware) => defaultMiddleware().concat(todosApi.middleware)
})

export default store

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch