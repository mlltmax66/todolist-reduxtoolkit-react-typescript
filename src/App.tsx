import React from 'react'
import { Switch, Route } from 'react-router'
import { TodoManager } from './features'

const App: React.FC = () => {
  return (
    <Switch>
      <Route path='/' component={TodoManager} />
    </Switch>
  );
}

export default App
